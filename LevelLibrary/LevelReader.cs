﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LevelLibrary
{
    class LevelReader : ContentTypeReader<LevelSerializable>
    {
        /// <summary>
        /// Loads an imported shader.
        /// </summary>
        protected override LevelSerializable Read(ContentReader input,
            LevelSerializable existingInstance)
        {
            string strLoad = input.ReadObject<string>();
            LevelSerializable tLevel = new LevelSerializable();
            tLevel.loadXML(strLoad);
            return tLevel;
        }
    }
}
