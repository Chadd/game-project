﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using LevelLibrary;

namespace LevelLibrary
{
    /// <summary>
    /// A device to facilitate the storage and retrieval of level data from a file
    /// </summary>
    [Serializable]
    public class LevelSerializable
    {
        // Do not change after we start using serialised levels!!!!!
        // Members must remain public to be serialised
        public string levelName = "";           // Name of the level
        public string levelDescription = "";    // A short description, such as "Volcano Madness"
        public string tilesetPath = "";         // The XNA content path to a tile set
        public string musicPath = "";           // The XNA content path to BGM
        public long rowLength;                  // The number of horizontal blocks in the level
        public LevelBlock[] lvlBlocks;          // The blocks which make up the level
        
        /// <summary>
        /// Constructs an empty level container
        /// </summary>
        public LevelSerializable()
        {
            
        }

        /// <summary>
        /// Creates a serialisable level object from the provided runtime level data
        /// </summary>
        /// <param name="blocks">A two-dimensional array of runtime data that you wish to serialise</param>
        public LevelSerializable(LevelBlock[,] blocks)
        {
            rowLength = blocks.GetLength(1);
            lvlBlocks = new LevelBlock[blocks.Length];
            int itterator = 0;
            foreach (LevelBlock item in blocks)
            {
                lvlBlocks[itterator] = item;
                itterator++;
            }
        }

        /// <summary>
        /// Returns runtime level data that is compatible with the old level drawing method
        /// </summary>
        /// <returns>A two-dimensional array of boolean values, in which true represents the position where a block should be drawn.</returns>
        /// <remarks>DEPRICATED</remarks>
        public Boolean[,] getDepricatedLevel()
        {
            long colLength = lvlBlocks.Length / rowLength;
            Boolean[,] blocks = new Boolean[colLength, rowLength];
            for (int i = 0; i < colLength; i++)
            {
                for (int j = 0; j < rowLength; j++)
                {
                    if (lvlBlocks[(i * rowLength) + j].playerBlock > 0) blocks[i, j] = true;
                }
            }
            return blocks;
        }

        /// <summary>
        /// Processes the serialisable level block data and returns a runtime block set
        /// </summary>
        /// <returns>A set of instructions on how to draw a level. Used by the level class to render a level.</returns>
        public LevelBlock[,] getLevelBlocks()
        {
            LevelBlock[,] blocks = new LevelBlock[lvlBlocks.Length / rowLength, rowLength];
            for (int i = 0; i < lvlBlocks.Length / rowLength; i++)
            {
                for (int j = 0; j < rowLength; j++)
                {
                    blocks[i,j] = lvlBlocks[i+j];
                }
            }
            return blocks;
        }

        /// <summary>
        /// Allows the use of older versions of runtime level data to be serialised.
        /// </summary>
        /// <param name="blocks">Old single texture block drawing data</param>
        /// <remarks>DEPRICATED</remarks>
        public LevelSerializable(Boolean[,] blocks)
        {
            rowLength = blocks.GetLength(1);
            lvlBlocks = new LevelBlock[rowLength * blocks.GetLength(0)];
            int itterator = 0;
            foreach (Boolean item in blocks)
            {
                if (item)
                {
                    lvlBlocks[itterator] = new LevelBlock();
                    lvlBlocks[itterator].playerBlock = 1;
                }
                else
                {
                    lvlBlocks[itterator] = new LevelBlock();
                    lvlBlocks[itterator].playerBlock = 0;
                }
                itterator++;
            }
        }

        /// <summary>
        /// Serialises and saves stored level data to an XML file
        /// </summary>
        /// <param name="fileName">The name of the file to save to</param>
        public void save(string fileName)
        {
            LevelSerializable tLevel = new LevelSerializable();
            tLevel.levelName = this.levelName;
            tLevel.levelDescription = this.levelDescription;
            tLevel.tilesetPath = this.tilesetPath;
            tLevel.musicPath = this.musicPath;
            tLevel.rowLength = this.rowLength;
            tLevel.lvlBlocks = this.lvlBlocks;

            Stream stream = null;
            try
            {
                stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(tLevel.GetType());
                x.Serialize(stream, tLevel);
            }
            catch
            {
                // do nothing, just ignore any possible errors
            }
            finally
            {
                if (null != stream)
                    stream.Close();
            }
        }

        /// <summary>
        /// Serialises this instance of level data to XML.
        /// </summary>
        /// <returns>Serialised XML</returns>
        public string getXML()
        {
            LevelSerializable tLevel = new LevelSerializable();
            tLevel.levelName = this.levelName;
            tLevel.levelDescription = this.levelDescription;
            tLevel.tilesetPath = this.tilesetPath;
            tLevel.musicPath = this.musicPath;
            tLevel.rowLength = this.rowLength;
            tLevel.lvlBlocks = this.lvlBlocks;

            StringWriter writer = new StringWriter();
            try
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(tLevel.GetType());
                x.Serialize(writer, tLevel);
            }
            catch
            {
                // do nothing, just ignore any possible errors
            }

            return writer.ToString();
        }

        /// <summary>
        /// Loads serialised level data from a supplied XML file
        /// </summary>
        /// <param name="strXML">A string containing the serialised level XML</param>
        public void loadXML(string strXML)
        {
            
            StringReader reader = new StringReader(strXML);
            LevelSerializable settings = new LevelSerializable();
            try
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(settings.GetType());
                settings = (LevelSerializable)x.Deserialize(reader);

                this.levelName = settings.levelName;
                this.levelDescription = settings.levelDescription;
                this.tilesetPath = settings.tilesetPath;
                this.musicPath = settings.musicPath;
                this.rowLength = settings.rowLength;
                this.lvlBlocks = settings.lvlBlocks;
            }
            catch
            {
                // do nothing, just ignore any possible errors
            }
            finally
            {

            }
        }

        /// <summary>
        /// Loads level data from an XML file
        /// </summary>
        /// <param name="fileName">The name of the file to load from</param>
        public void load(string fileName)
        {
            Stream stream = null;
            LevelSerializable settings = new LevelSerializable();
            try
            {
                stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(settings.GetType());
                settings = (LevelSerializable)x.Deserialize(stream);

                this.levelName = settings.levelName;
                this.levelDescription = settings.levelDescription;
                this.tilesetPath = settings.tilesetPath;
                this.musicPath = settings.musicPath;
                this.rowLength = settings.rowLength;
                this.lvlBlocks = settings.lvlBlocks;
            }
            catch
            {
                // do nothing, just ignore any possible errors
            }
            finally
            {
                if (null != stream)
                    stream.Close();
            }
        }
    }

    /// <summary>
    /// Defines a multi-layered LevelBlock object which can be used to draw a portion of a level.
    /// </summary>
    public class LevelBlock
    {
        // Holds block ID's
        public byte backgroundBlock = 0;
        public byte playerBlock = 0;
        public byte foregroundBlock = 0;

        // Holds item types
        public Type backgroundObject = null;
        public Type playerObject = null;
        public Type ForegroundObject = null;

        /// <summary>
        /// Creates an empty block
        /// </summary>
        public LevelBlock() { }

        /// <summary>
        /// Creates a LevelBlock which will use the specified texture value
        /// </summary>
        /// <param name="pBlock">A block ID that will be used at the player depth level</param>
        public LevelBlock(byte pBlock)
        {
            playerBlock = pBlock;
        }
    }
}
