﻿using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DolphinGame
{

    class Player : GameObject
    {
        ContentManager content;
        int maxSpeed = 5;
        float accelRate = 0.1F;
        float waterDrag = 0.05F;

        public Player()
        {
        }

        public override void handleMovement(KeyboardState input)
        {
            //Using key W, move player in the negative Y direction
            //while player is not colliding with an object above itself
            if (input.IsKeyDown(Keys.W) && collidingWith != Collision.top
                && collidingWith != Collision.topANDleft
                && collidingWith != Collision.topANDright)
            {
                if (-maxSpeed <= momentum.Y - accelRate) momentum.Y -= accelRate;
            }

            //Using key S, move player in the positive Y direction
            //while player is not colliding with an object below itself
            if (input.IsKeyDown(Keys.S) && collidingWith != Collision.bottom
                && collidingWith != Collision.bottomANDleft
                && collidingWith != Collision.bottomANDright)
            {
                if (maxSpeed >= momentum.Y + accelRate) momentum.Y += accelRate;
            }

            //Using key A, move player in the negative X direction
            //while player is not colliding with an object to its left
            if (input.IsKeyDown(Keys.A) && collidingWith != Collision.left
                && collidingWith != Collision.topANDleft
                && collidingWith != Collision.bottomANDleft)
            {
                if (-maxSpeed <= momentum.X - accelRate) momentum.X -= accelRate;
            }

            //using key D, move player in the positive X direction
            //while player is not colliding with an object to its right
            if (input.IsKeyDown(Keys.D) && collidingWith != Collision.right
                && collidingWith != Collision.topANDright
                && collidingWith != Collision.bottomANDright)
            {
                if (maxSpeed >= momentum.X + accelRate) momentum.X += accelRate;
            }

            //This key command is being used for level testing
            if (input.IsKeyDown(Keys.R) && collidingWith != Collision.right)
            {
                Level.reset();
            }
        }

        public override void Update(GameTime gameTime)
        {
            //Stop moving in a direction if we have collided with a platform
            #region "Collision Handling"
            switch (this.collidingWith)
            {
                case Collision.top:
                    momentum.Y = 0;
                    position.Y += 1;
                    break;
                case Collision.right:
                    momentum.X = 0;
                    position.X -= 1;
                    break;
                case Collision.bottom:
                    momentum.Y = 0;
                    position.Y -= 1;
                    break;
                case Collision.left:
                    momentum.X = 0;
                    position.X += 1;
                    break;
                case Collision.bottomANDleft:
                    momentum.Y = 0;
                    momentum.X = 0;
                    position.Y -= 1;
                    position.X += 1;
                    break;
                case Collision.bottomANDright:
                    momentum.Y = 0;
                    momentum.X = 0;
                    position.Y -= 1;
                    position.X -= 1;
                    break;
                case Collision.topANDleft:
                    momentum.Y = 0;
                    momentum.X = 0;
                    position.Y += 1;
                    position.X += 1;
                    break;
                case Collision.topANDright:
                    momentum.Y = 0;
                    momentum.X = 0;
                    position.Y += 1;
                    position.X -= 1;
                    break;
            }
            #endregion "Collision Handling"

            //Apply drag to momentum
            //First perform operations on Y position
            if (momentum.Y != 0)
            {
                //determine if momentum is negative or positive
                if (momentum.Y > 0)
                {
                    //If applying drag would change direction of momentum, then set
                    //momentum to zero
                    if (momentum.Y - waterDrag < 0)
                    {
                        momentum.Y = 0;
                    }
                    else
                    {
                        momentum.Y -= waterDrag;
                    }
                }
                else
                {
                    //If applying drag would change direction of momentum, then set
                    //momentum to zero
                    if (momentum.Y + waterDrag > 0)
                    {
                        momentum.Y = 0;
                    }
                    else
                    {
                        momentum.Y += waterDrag;
                    }
                }
            }

            //Lastly, test for X component of momentum
            if (momentum.X != 0)
            {
                //determine if momentum is negative or positive
                if (momentum.X > 0)
                {
                    //If applying drag would change direction of momentum, then set
                    //momentum to zero
                    if (momentum.X - waterDrag < 0)
                    {
                        momentum.X = 0;
                    }
                    else
                    {
                        momentum.X -= waterDrag;
                    }
                }
                else
                {
                    //If applying drag would change direction of momentum, then set
                    //momentum to zero
                    if (momentum.X + waterDrag > 0)
                    {
                        momentum.X = 0;
                    }
                    else
                    {
                        momentum.X += waterDrag;
                    }
                }
            }

            //Use momentum to change position
            this.setPosition(this.getPosition() + momentum);
        }

        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(Level.Game.Services, "Content");

            texture = content.Load<Texture2D>(@"textures\player\player_right");

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            Level.Game.ResetElapsedTime();
        }

        //Since all GameObjects need to be able to be drawn, I have moved the draw function into the parent class
    }
}
