﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace DolphinGame
{
    //Creates the definition for the enumerator Collision
    public enum Collision
    {
        none,
        top,
        bottom,
        left,
        right,
        topANDright,
        topANDleft,
        bottomANDright,
        bottomANDleft
    }

    public abstract class GameObject
    {
        //Ye Olde variables
        //private int momentum;
        protected Vector2 momentum;
        private bool gravity;
        private Vector2 dimensions;
        protected Vector2 position;
        private int useInput;
        protected Texture2D texture;
        protected Collision collidingWith = Collision.none;

        public Level Level
        {
            get { return level; }
            internal set { level = value; }
        }

        Level level;

        public virtual void LoadContent() { }
        public virtual void UnloadContent() { }

        public virtual void Update(GameTime gameTime) { }

        public virtual void handleMovement(KeyboardState input) { }

        public void Draw(GameTime gameTime) 
        {
            GraphicsDevice graphics = Level.GraphicsDevice;
            SpriteBatch spriteBatch = Level.SpriteBatch;

            spriteBatch.Begin();
            spriteBatch.Draw(texture, getPosition() - Level.getCamera(), Color.White);
            spriteBatch.End();
        }

        /// <summary>
        /// Sets the objects Vector2 Position
        /// </summary>
        /// <param name="pVector">Vector2</param>
        public void setPosition(Vector2 pVector)
        {
            position = pVector;
        }

        /// <summary>
        /// Sets the objects Vector2 Position
        /// </summary>
        /// <param name="pX">int X Position</param>
        /// <param name="pY">int Y Position</param>
        public void setPosition(int pX, int pY)
        {
            position.X = pX;
            position.Y = pY;
        }

        /// <summary>
        /// Returns the position of the game object.
        /// </summary>
        /// <returns>Vector2</returns>
        public Vector2 getPosition()
        {
            return position;
        }

        /// <summary>
        /// Sets the Texture of the object
        /// </summary>
        /// <param name="pTexture">Texture2D object</param>
        public void setTexture(Texture2D pTexture)
        {
            texture = pTexture;
        }
        public Vector2 getTextureDimensions()
        {
            Vector2 tempVector = new Vector2(texture.Width, texture.Height);
            return tempVector;
        }
        public void collidingState(Collision cSide)
        {
            collidingWith = cSide;
        }
        public Collision lastCollidingState()
        {
            return collidingWith;
        }
    }
}
