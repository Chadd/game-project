
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DolphinGame
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        Level level;

        static readonly string[] preloadAssets =
        {
            "default",
        };

        public Game1()
        {
            Content.RootDirectory = "Content";

            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;

            // Create the screen manager component.
            level = new Level(this);
            Components.Add(level);
            level.load(Content.Load<string>(@"levels\0"));
        }

        protected override void LoadContent()
        {
            foreach (string asset in preloadAssets)
            {
                Content.Load<object>(asset);
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);
        }
        
    }
}
