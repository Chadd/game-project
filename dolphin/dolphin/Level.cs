﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using LevelLibrary;

namespace DolphinGame
{
    /// <summary>
    /// Defines the functionality of a level and handles drawing and updating of level objects.
    /// </summary>
    /// <remarks></remarks>
    public class Level : DrawableGameComponent
    {
        Boolean[,] bolPlatforms;
        List<GameObject> levelObjects = new List<GameObject>();
        List<GameObject> levelObjectsUpdatable = new List<GameObject>();
        InputState input = new InputState();
        SpriteBatch spriteBatch;
        bool isInitialized;
        Texture2D texDefault;
        Texture2D texBackground;
        Texture2D[] textures;
        string lvlString;
        Vector2 camera = new Vector2(0,0);

        /// <summary>
        /// Allows access to a unified SpriteBatch for all level objects
        /// </summary>
        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        /// <summary>
        /// Builds the object that represents an instance of a level file. You must call load() before the level
        /// will begin to be drawn.
        /// </summary>
        /// <param name="game"></param>
        public Level(Game game)
            : base(game)
        {
        }

        /// <summary>
        /// Called once the level is added to the components list. The Draw() and Update() methods will not be
        /// called until the object is fully initialized.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
            isInitialized = true;
        }

        /// <summary>
        /// Loads base content and sets up local SpriteBatch. Also loads content for any currently registered level objects.
        /// </summary>
        protected override void LoadContent()
        {
            ContentManager content = Game.Content;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            texDefault = content.Load<Texture2D>(@"textures/blocks/0");
            texBackground = content.Load<Texture2D>(@"test_background");

            //Load testing level using new content pipeline
            //LevelSerializable tLevel = content.Load<LevelSerializable>(@"levels/test");
            //bolPlatforms = tLevel.getDepricatedLevel();

            //Load new textures
            textures = new Texture2D[8];
            textures[0] = content.Load<Texture2D>(@"textures/blocks/0");
            textures[1] = content.Load<Texture2D>(@"textures/blocks/1");
            foreach (GameObject item in levelObjects)
            {
                item.LoadContent();
            }

        }

        /// <summary>
        /// Unloads content used by registered level objects.
        /// </summary>
        protected override void UnloadContent()
        {
            // Tell each of the screens to unload their content.
            foreach (GameObject item in levelObjects)
            {
                item.UnloadContent();
            }
        }

        /// <summary>
        /// Updates the state of the level and calls Update() for each registered level object.
        /// </summary>
        /// <param name="gameTime">The instance of the game that the level belongs to</param>
        public override void Update(GameTime gameTime)
        {
            // Calculate and update collisions with platforms
            Vector2 itemPos = new Vector2(0, 0);
            Vector2 itemSprite = new Vector2(0, 0);
            Rectangle itemArea = new Rectangle(0, 0, 0, 0);
            Rectangle testArea = new Rectangle(0, 0, 0, 0);
            foreach (GameObject item in levelObjects)
            {
                //flags for aggrigating collisions
                Boolean bolNorth = false;
                Boolean bolEast = false;
                Boolean bolSouth = false;
                Boolean bolWest = false;

                itemPos = item.getPosition();
                itemSprite = item.getTextureDimensions();
                itemArea = new Rectangle((int)itemPos.X, (int)itemPos.Y, (int)itemSprite.X, (int)itemSprite.Y);

                //Get block array size
                int intRows = bolPlatforms.GetLength(0);
                int intCols = bolPlatforms.GetLength(1);
                for (int i = 0; i < intRows; i++)
                {
                    for (int j = 0; j < intCols; j++)
                    {
                        if (this.getBlock(i, j))
                        {
                        testArea = new Rectangle(j * 50, i * 30, 50, 30);
                        if (itemArea.Intersects(testArea))
                        {
                            
                            //Calculate 4 points for collision detection
                            Vector2 vecNorth = new Vector2(itemArea.X + (itemArea.Width / 2), itemArea.Y);
                            Vector2 vecEast = new Vector2(itemArea.X + itemArea.Width, itemArea.Y + (itemArea.Height / 2));
                            Vector2 vecSouth = new Vector2(itemArea.X + (itemArea.Width / 2), itemArea.Y + itemArea.Height);
                            Vector2 vecWest = new Vector2(itemArea.X, itemArea.Y + (itemArea.Height / 2));
                            
                            //Aggrigate collisions
                            if (!bolNorth) bolNorth = testArea.Intersects(new Rectangle((int)vecNorth.X, (int)vecNorth.Y, 1, 1));
                            if (!bolEast) bolEast = testArea.Intersects(new Rectangle((int)vecEast.X, (int)vecEast.Y, 1, 1));
                            if (!bolSouth) bolSouth = testArea.Intersects(new Rectangle((int)vecSouth.X, (int)vecSouth.Y, 1, 1));
                            if (!bolWest) bolWest = testArea.Intersects(new Rectangle((int)vecWest.X, (int)vecWest.Y, 1, 1));

                        }
                        }
                    }

                }

                //Use some logic to set collision direction
                if (bolNorth)
                {
                    if (bolEast)
                    {
                        item.collidingState(Collision.topANDright);
                    }
                    else if (bolWest)
                    {
                        item.collidingState(Collision.topANDleft);
                    }
                    else
                    {
                        item.collidingState(Collision.top);
                    }
                }
                else if (bolEast)
                {
                    if (bolNorth)
                    {
                        item.collidingState(Collision.topANDright);
                    }
                    else if (bolSouth)
                    {
                        item.collidingState(Collision.bottomANDright);
                    }
                    else
                    {
                        item.collidingState(Collision.right);
                    }
                }
                else if (bolSouth)
                {
                    if (bolEast)
                    {
                        item.collidingState(Collision.bottomANDright);
                    }
                    else if (bolWest)
                    {
                        item.collidingState(Collision.bottomANDleft);
                    }
                    else
                    {
                        item.collidingState(Collision.bottom);
                    }
                }
                else if (bolWest)
                {
                    if (bolNorth)
                    {
                        item.collidingState(Collision.topANDleft);
                    }
                    else if (bolSouth)
                    {
                        item.collidingState(Collision.bottomANDleft);
                    }
                    else
                    {
                        item.collidingState(Collision.left);
                    }
                }
                else
                {
                    item.collidingState(Collision.none);
                }

            }
            // Read the keyboard and gamepad.
            input.Update();

            //Perform camera calculations
            //First, find position of player object
            Vector2 playerPos = new Vector2(0, 0);
            Type tPlayer = Type.GetType("DolphinGame.Player");
            foreach (GameObject item in levelObjects)
            {
                if (item.GetType() == tPlayer) playerPos = item.getPosition();
            }

            //Determine if we need to make camera adjustments
            //Check horizontal first
            if (playerPos.X - camera.X > 1000)
            {
                camera.X += 5;
            }
            else if (playerPos.X - camera.X < 280)
            {
                camera.X -= 5;
            }

            //Check vertical and make final adjustments
            if (playerPos.Y - camera.Y > 620)
            {
                camera.Y += 5;
            }
            else if (playerPos.Y - camera.Y < 100)
            {
                camera.Y -= 5;
            }

            //Call Update for registered level objects
            foreach(GameObject item in levelObjects)
            {
                KeyboardState kState = Keyboard.GetState();
                item.handleMovement(kState);
                item.Update(gameTime);
            }
        }

        /// <summary>
        /// Draws the level, including each registered level object.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            //Draw level
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            spriteBatch.Draw(texBackground, new Vector2(0,0), null, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
            int intRows = bolPlatforms.GetLength(0);
            int intCols = bolPlatforms.GetLength(1);
            for (int i = 0; i < intRows; i++)
            {
                for (int j = 0; j < intCols; j++)
                {

                    if (this.getBlock(i, j)) spriteBatch.Draw(texDefault, new Vector2(j * 50, i * 30) - camera, Color.White);
                }
            }

            spriteBatch.End();
            foreach (GameObject item in levelObjects)
            {
                item.Draw(gameTime);
            }
        }

        /// <summary>
        /// Allows the reading of indavidual block states
        /// </summary>
        /// <param name="row">Row which the block resides</param>
        /// <param name="col">Column in which the block resides</param>
        /// <returns>The state of the block.</returns>
        public Boolean getBlock(int row, int col)
        {
            return this.bolPlatforms[row, col];
        }

        /// <summary>
        /// Loads and parses the level file.
        /// </summary>
        /// <param name="strLevel"></param>
        public void load(string strLevel)
        {
            //Keep a copy of the level string for resets
            lvlString = strLevel;

            ///screen.ControllingPlayer = controllingPlayer;
            ///screen.ScreenManager = this;
            ///screen.IsExiting = false;

            //Prepare object list to recieve objects
            levelObjects = new List<GameObject>();

            string[] strLines;
            strLines = strLevel.Split('\n');

            //get dimensions from first line
            //Should be in the format ###x###
            string trmDimentions = strLines[1].Trim();
            string[] strDimensions = trmDimentions.Split('x');
            int gridHeight = Convert.ToInt32(strDimensions[0]);
            int gridWidth = Convert.ToInt32(strDimensions[1]);

            this.bolPlatforms = new Boolean[gridWidth, gridHeight];

            int itterateRow = 0;
            int itterateCol = 0;

            foreach (string strRow in strLines)
            {
                if (strRow.Length >= gridWidth)
                {
                    //reset column counter
                    itterateCol = 0;

                    string strTrimmedRow = "";
                    strTrimmedRow = strRow.Trim();
                    foreach (char chrCell in strTrimmedRow)
                    {
                        switch (chrCell)
                        {
                            case '0':
                                this.bolPlatforms[itterateRow, itterateCol] = false;
                                itterateCol++;
                                break;
                            case '1':
                                this.bolPlatforms[itterateRow, itterateCol] = true;
                                itterateCol++;
                                break;
                            case 'P':
                                Player player = new Player();
                                player.setPosition(new Vector2(itterateCol * 50, itterateRow * 30));
                                player.Level = this;
                                if (isInitialized) player.LoadContent();
                                levelObjects.Add(player);

                                //Also add blank space to the block array
                                this.bolPlatforms[itterateRow, itterateCol] = false;
                                itterateCol++;
                                break;
                            case 'E':
                                Enemy enemy = new Enemy();
                                enemy.setPosition(new Vector2(itterateCol * 50, itterateRow * 30));
                                enemy.Level = this;
                                if (isInitialized) enemy.LoadContent();
                                levelObjects.Add(enemy);

                                //Also add blank space to the block array
                                this.bolPlatforms[itterateRow, itterateCol] = false;
                                itterateCol++;
                                break;
                            default:
                                this.bolPlatforms[itterateRow, itterateCol] = false;
                                break;
                        }
                    }

                    itterateRow++;
                }


            }
            
            //test serialization
            //LevelSerializable lvlTest = new LevelSerializable(bolPlatforms);
            //lvlTest.save("HURAY");
            //LevelSerializable tLoad = new LevelSerializable();
            //tLoad.load("HURAY");
            //bolPlatforms = tLoad.getDepricatedLevel();
        }

        /// <summary>
        /// Resets the state of the level to the default state stored in the level file.
        /// </summary>
        /// <remarks>Currently broken</remarks>
        public void reset()
        {
            levelObjects.Clear();
            this.load(lvlString);
        }

        public Vector2 getCamera()
        {
            return camera;
        }

        
    }

}
