﻿using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DolphinGame
{

    class Enemy : GameObject
    {
        ContentManager content;

        private String name;
        private int health;

        public Enemy()
        {

        }

        public void setName(String pName)
        {
            name = pName;
        }

        public String getName()
        {
            return name;
        }

        public void setHealth(int pHealth)
        {
            health = pHealth;
        }

        public int getHealth()
        {
            return health;
        }

        public override void handleMovement(KeyboardState input)
        {

        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(Level.Game.Services, "Content");

            texture = content.Load<Texture2D>(@"textures\enemies\shark");

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            Level.Game.ResetElapsedTime();
        }

        //Since all GameObjects need to be able to be drawn, I have moved the draw function into the parent class GameObject
    }
}
