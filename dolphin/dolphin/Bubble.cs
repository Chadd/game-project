﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DolphinGame
{

    class Bubble : GameObject
    {

        //Would it be better to make children of the Player class, such as Weapon and Item then derive specific weapons and items from that class?

        public Bubble()
        {
        }

        public override void handleMovement(KeyboardState input)
        {
        }

        public override void Update(GameTime gameTime)
        {

        }

        //Since all GameObjects need to be able to be drawn, I have moved the draw function into the parent class
    }
}
