using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using LevelLibrary;

namespace SerialisedProcessorLib
{
    [ContentProcessor(DisplayName = "Pixel Shader Processor")]
    class LevelProcessor : ContentProcessor<LevelSerializable, LevelSerializable>
    {
        public override LevelSerializable Process(LevelSerializable input,
            ContentProcessorContext context)
        {
            // In the future, we may decide to do some processing. But for now,
            // just pass along what we already have.
            return input;
        }
    }
}
