using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using LevelLibrary;

namespace SerialisedProcessorLib
{
    [ContentTypeWriter]
    class LevelWriter : ContentTypeWriter<LevelSerializable>
    {
        protected override void Write(ContentWriter output, LevelSerializable value)
        {
            string strOut = value.getXML();
            output.WriteObject<string>(strOut);
        }
        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(LevelSerializable).AssemblyQualifiedName;
        }
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "LevelLibrary.LevelReader, LevelLibrary," +
                " Version=1.0.0.0, Culture=neutral";
        }
    }
}
