using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using LevelLibrary;

namespace SerialisedProcessorLib
{
    [ContentImporter(".lvl", DisplayName = "Dolphin Level (.lvl)", DefaultProcessor = "LevelProcessor")]
    class LevelImporter : ContentImporter<LevelSerializable>
    {
        public override LevelSerializable Import(string filename,
            ContentImporterContext context)
        {
            LevelSerializable tLevel = new LevelSerializable();
            tLevel.load(filename);
            return tLevel;
        }
    }
}
